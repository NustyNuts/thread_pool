#include "executor.h"
#include "simple_divisor_search.h"
#include <exception>
#include <iostream>
#include <sstream>
#include <string>

namespace app {
namespace {
int thread_count = DEFAULT_THREAD_COUNT;
int thread_increment = THREAD_INCREMENT;
} // namespace
executor_t::executor_t() noexcept
    : thread_pool(std::make_unique<::utils::thread::pool_t>()) {
  thread_pool->start(thread_count);
  std::cout << "tread pool started with 1 thread" << std::endl;
}

int executor_t::execute() noexcept {
  try {
    std::string user_command;
    while (user_command != "exit") {
      switch (input_processing()) {
      case task_type_e::exit:
        return 0;
        break;
      case task_type_e::restart:
        break;
      case task_type_e::calculate:
        break;
      default:
        std::cout << "Wrong input!" << std::endl;
        continue;
      }
    }

  } catch (const std::exception &e) {
    std::cerr << e.what() << '\n';
    return -1;
  }
  return 0;
}

executor_t::task_type_e executor_t::input_processing() noexcept {
  std::string task;
  std::cout << "Enter command or number: " << std::endl;
  std::getline(std::cin, task);
  if (task == "exit") {
    return task_type_e::exit;
  } else if (task == "restart") {
    return restart_thread_pool();
  } else {
    return calculate_task(task);
  }
  return task_type_e::error;
}

executor_t::task_type_e executor_t::calculate_task(std::string task) noexcept {
  try {
    uint64_t number = std::stoull(task);
    std::cout << "Enter priority: " << std::endl;
    std::getline(std::cin, task);
    uint32_t priority = std::stoul(task);
    static std::atomic<int> cb_count = 0;
    static int task_num = 1;
    task::simple_divisor_search_t::result_cb_t print_result_cb =
        [current_task_num = task_num](std::set<uint64_t> result) {
          std::stringstream ss;
          for (const auto &devisor : result)
            ss << devisor << " ";
          std::cout << "result for task# " << current_task_num
                    << " result: " << ss.str() << std::endl;
        };
    task::simple_divisor_search_t::ptr task =
        std::make_shared<task::simple_divisor_search_t>(
            std::move(priority), std::move(number), std::move(print_result_cb));
    thread_pool->appendTask(std::move(task));

    return task_type_e::calculate;
  } catch (...) {
  }
  return task_type_e::error;
}

executor_t::task_type_e executor_t::restart_thread_pool() noexcept {
  thread_pool.reset(new ::utils::thread::pool_t());
  thread_count += thread_increment;
  std::cout << "start new pool with thread count: " << thread_count
            << std::endl;
  thread_pool->start(thread_count);
  return task_type_e::restart;
}
} // namespace app