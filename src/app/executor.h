#pragma once
#include <memory>
#include <utils/threads/include/thread_pool.h>

namespace app {
class executor_t {

public:
  enum class task_type_e { exit, restart, calculate, error };

public:
  executor_t() noexcept;
  int execute() noexcept;

private:
  task_type_e input_processing() noexcept;
  task_type_e calculate_task(std::string task) noexcept;
  task_type_e restart_thread_pool() noexcept;

private:
  std::unique_ptr<::utils::thread::pool_t> thread_pool;
};
} // namespace app