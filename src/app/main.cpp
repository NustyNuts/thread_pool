#include <algorithm>
#include <cstddef>
#include <iostream>
#include <string>

#include "executor.h"

int main() {
  app::executor_t app;
  return app.execute();
}
