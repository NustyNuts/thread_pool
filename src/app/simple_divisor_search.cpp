#include "simple_divisor_search.h"
#include <cmath>
#include <iostream>
#include <thread>


namespace app {
namespace task {
namespace {
using namespace std::chrono_literals;

} // namespace

simple_divisor_search_t::simple_divisor_search_t(
    uint32_t priority, uint64_t number, result_cb_t &&result_cb) noexcept
    : utils::task::base_t(priority), number(number),
      result_cb(std::move(result_cb)) {}

void simple_divisor_search_t::run() noexcept {
  std::set<uint64_t> result;
#ifdef TASK_TIME_DELAY_SEK
  std::this_thread::sleep_for(std::chrono::seconds(TASK_TIME_DELAY_SEK));
#endif
  uint64_t i = 2;
  for (uint64_t i = 2; i < std::sqrt(number);) {
    if (interrupt) {
      result_cb({});
      return;
    }
    if (number % i == 0) {
      result.insert(i);
      number /= i;
    } else {
      ++i;
    }
  }
  if (number > 1)
    result.insert(number);
  result_cb(std::move(result));
}

} // namespace task
} // namespace app
