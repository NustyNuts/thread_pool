#pragma once
#include <set>
#include <utils/threads/include/base_task.h>
namespace app {
namespace task {
class simple_divisor_search_t : public utils::task::base_t {

public:
  using result_cb_t =
      std::function<void(const std::set<uint64_t> &simple_divisors)>;
  simple_divisor_search_t(uint32_t priority, uint64_t number,
                          result_cb_t &&result_cb) noexcept;
  virtual void run() noexcept override;

private:
  std::function<void(std::set<uint64_t>)> result_cb;
  uint64_t number;
};
} // namespace task
} // namespace app