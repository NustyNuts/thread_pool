#pragma once
#include <atomic>
#include <functional>
#include <memory>

namespace utils {
namespace task {

class base_t {
public:
  using const_ptr = std::shared_ptr<const base_t>;
  using ptr = std::shared_ptr<base_t>;

public:
  base_t(uint32_t priority) noexcept;
  virtual void run() noexcept = 0;
  uint32_t getPriority() const noexcept;
  void interruptTask() noexcept;
  friend inline bool operator<(const base_t &left, const base_t &right) {
    return right.priority > left.priority;
  }
  friend inline bool operator>(const base_t &left, const base_t &right) {
    return right.priority < left.priority;
  }
  friend inline bool operator<=(const base_t &left, const base_t &right) {
    return right.priority <= left.priority;
  }
  friend inline bool operator>=(const base_t &left, const base_t &right) {
    return right.priority >= left.priority;
  }

public:
  class ptr_comparator {
  public:
    bool operator()(base_t::ptr a, base_t::ptr b) {
      return (*a.get()) < (*b.get());
    }
    bool operator()(base_t::const_ptr a, base_t::ptr b) {
      return a->priority < b->priority;
    }
  };

protected:
  std::atomic<bool> interrupt;
  uint32_t priority;
};
} // namespace task
} // namespace utils
