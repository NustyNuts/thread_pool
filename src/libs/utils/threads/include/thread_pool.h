#pragma once
#include <condition_variable>
#include <deque>
#include <map>
#include <queue>
#include <utils/threads/include/base_task.h>
#include <thread>

namespace utils {
namespace thread {
class pool_t {
public:
  enum class state_e { idle, starting, stopping, active };

public:
  pool_t() noexcept;
  ~pool_t() noexcept;
  void appendTask(task::base_t::ptr task) noexcept;
  // thread_count < 0 - preffered thread count
  bool start(int32_t thread_count) noexcept;
  void stop() noexcept;

private:
  void supervisor_proc() noexcept;
  void proc_supervisor_starting() noexcept;
  void proc_supervisor_stopping() noexcept;
  void worker_proc() noexcept;

private:
  std::priority_queue<task::base_t::ptr, std::vector<task::base_t::ptr>,
                      task::base_t::ptr_comparator>
      tasks_queue;
  std::map<std::thread::id, task::base_t::ptr> executing_tasks;
  std::mutex mutex;
  std::condition_variable supervisor_condition;
  std::condition_variable worker_condition;
  std::vector<std::thread> worker_threads;
  std::thread supervisor_thread;
  std::atomic<state_e> state;
};
} // namespace thread
} // namespace utils
