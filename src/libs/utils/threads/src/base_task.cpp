#include <utils/threads/include/base_task.h>
namespace utils {
namespace task {

base_t::base_t(uint32_t priority) noexcept : priority(priority) {}

uint32_t base_t::getPriority() const noexcept { return priority; }

void base_t::interruptTask() noexcept { interrupt = true; }

} // namespace task
} // namespace utils