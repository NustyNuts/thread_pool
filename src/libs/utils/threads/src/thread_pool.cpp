#include <cassert>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <utils/threads/include/thread_pool.h>

namespace utils {
namespace thread {
namespace {

void print_info(std::string info) {
#ifdef BUILD_WITH_DEBUG_INFO
  std::cout << info << std::endl;
#endif
}

} // namespace

pool_t::pool_t() noexcept {}

pool_t::~pool_t() noexcept {
  stop();
  if (state.exchange(state_e::idle) != state_e::idle) {
    worker_condition.notify_all();
    supervisor_thread.join();
  }
}
bool pool_t::start(int thread_count) noexcept {
  if (state != state_e::idle) {
    print_info("error while startind thread pool, current state dont idle!");
    assert(false);
    return false;
  }
  if (thread_count < 0) {
    worker_threads.resize(std::thread::hardware_concurrency());
  } else {
    worker_threads.resize(thread_count);
  }
  state = state_e::starting;
  std::unique_lock<std::mutex> lock(mutex);
  supervisor_thread = std::thread(&pool_t::supervisor_proc, this);
  lock.unlock();
  while (state != state_e::active)
    std::this_thread::yield();
  return true;
}

void pool_t::stop() noexcept {
  switch (state) {
  case state_e::idle:
    print_info("already stopped.");
    return;
  case state_e::active:
    print_info("stopping thread pool ...");
    break;
  case state_e::starting:
    print_info("error while stoppint, wrong state - starting");
    return;
  case state_e::stopping:
    print_info("error while stoppint, wrong state - stopping");
    return;
  }
  state = state_e::stopping;
  std::unique_lock<std::mutex> lock(mutex);
  supervisor_condition.notify_one();
  lock.unlock();
  supervisor_thread.join();
  print_info("supervisor thread stopped");
}

void pool_t::appendTask(task::base_t::ptr task) noexcept {
  if (!task) {
    print_info("error-try add null ptr task");
    return;
  }
  std::lock_guard<std::mutex> lock(mutex);
  tasks_queue.push(task);
  worker_condition.notify_one();
}

void pool_t::supervisor_proc() noexcept {
  try {

    while (state != state_e::idle) {
      switch (state) {
      case state_e::starting:
        proc_supervisor_starting();
        continue;
      case state_e::stopping:
        proc_supervisor_stopping();
        continue;
      case state_e::active:
        continue;
      }
      assert(false);
      state = state_e::stopping;
      std::unique_lock<std::mutex> lock(mutex);
      worker_condition.notify_all();
    }
  } catch (const std::exception &e) {
    assert(false);
    print_info("exception in supervisor thread" + std::string(e.what()));
  } catch (...) {
    assert(false);
    print_info("unknow exception in supervisor thread");
  }
}

void pool_t::proc_supervisor_starting() noexcept {
  try {
    std::unique_lock<std::mutex> lock(mutex);
    for (auto &thread : worker_threads)
      thread = std::thread(std::bind(&pool_t::worker_proc, this));
    state = state_e::active;
  } catch (const std::exception &e) {
    print_info("error while starting threads " + std::string(e.what()));
  } catch (...) {
    print_info("unknow exception while starting threads");
  }
}
void pool_t::proc_supervisor_stopping() noexcept {
  print_info("stoping threads...");
  std::unique_lock<std::mutex> lock(mutex);
  worker_condition.notify_all();
  auto isLocked = lock.owns_lock();
  lock.unlock();
  for (const auto &task : executing_tasks) {
    if (task.second) {
      std::stringstream ss;
      ss << "send interupt for task in thread: ";
      ss << task.first;
      print_info(ss.str());
      task.second->interruptTask();
    }
  }
  for (auto &thread : worker_threads)
    if (thread.joinable())
      thread.join();
  print_info("all thread finished, clear...");
  auto tasks = std::move(tasks_queue);
  lock.lock();
  worker_threads.clear();
  executing_tasks.clear();
  lock.unlock();
  while (!tasks.empty())
    tasks.pop();
  lock.lock();
  state = state_e::idle;
}

void pool_t::worker_proc() noexcept {

  while (state == state_e::active) {
    std::unique_lock<std::mutex> lock(mutex);
    if (tasks_queue.empty()) {
      worker_condition.wait(lock, [&]() {
        return !tasks_queue.empty() || state != state_e::active;
      });
    } else {

      auto task = tasks_queue.top();
      print_info("getting task with priority: " +
                 std::to_string(task->getPriority()));
      tasks_queue.pop();
      executing_tasks[std::this_thread::get_id()] = task;
      lock.unlock();
      try {
        task->run();
      } catch (const std::exception &e) {
        print_info("error in executing task: " + std::string(e.what()));
      }
      executing_tasks[std::this_thread::get_id()] = nullptr;
    }
  }
}
} // namespace thread
} // namespace utils
